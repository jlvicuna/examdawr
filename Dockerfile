# For Java 17, 
FROM openjdk:17.0.2-jdk-slim-buster
LABEL MAINTAINER="kjimenezp@uees.edu.ec"

WORKDIR /opt/app

ARG JAR_FILE=pet-clinic-1.0.0.jar

# Copy the spring-boot-api-tutorial.jar from the maven stage to the /opt/app directory of the current stage.
COPY target/${JAR_FILE} /opt/app/

EXPOSE 8080

ENTRYPOINT ["java","-jar","pet-clinic-1.0.0.jar"]